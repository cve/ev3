% Formeln

## Gefahrene Stecke auf Rad

$$ s = \frac{T}{360} \cdot U $$

* $s$: Gefahrene Strecke
* $T$: Ticks (Tacho) des Rades
* $U$: Umfang des Rades

## Auf der Stelle rotieren

$$ T = \frac{a}{d} \cdot \alpha $$

* $T$: Ticks
* $a$: Abstand der Räder
* $d$: Durchmesser der Räder
* $\alpha$: Rotationswinkel

## Kurve fahren

$$ T_A = \frac{(2r - a) \cdot \alpha}{d} $$
$$ T_I = \frac{(2r + a) \cdot \alpha}{d} $$
$$ v_A = \frac{v_N}{(\frac{T_A}{T_I})} $$

* $T_A$: Ticks für das äußere Rad
* $T_I$: Ticks für das innere Rad
* $r$: Radius des Kurvenkreisbogens
* $a$: Abstand der Räder
* $\alpha$: Winkel des Kurvenkreisbogens
* $d$: Durchmesser der Räder
* $v_N$: Standardgeschwindigkeit
* $v_A$: Geschwindigkeit für das äußere Rad

## Über dieses Dokument

Dieses Dokument wurde in Markdown geschrieben und lässt sich
[hier](https://gitli.stratum0.org/emilengler/ev3) einsehen.

Für die Generierung dieser Ausgabe wurde folgender Befehl verwendet:
```sh
pandoc -s -f markdown --webtex -t html ev3.md > ev3.html
```
